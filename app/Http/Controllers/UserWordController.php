<?php

namespace App\Http\Controllers;

use App\Models\{User, Word};
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class UserWordController extends Controller
{
    /**
     * Display the user's words
     */
    public function index(User $user): Response
    {
        return Inertia::render('Words/Index', [
            'words' => $user->words()->with('user')->latest()->get(),
        ]);
    }

    /**
     * Display a form for posting a new word
     */
    public function create(): Response
    {
        return Inertia::render('Words/Create');
    }

    /**
     * Validate and store the new word
     */
    public function store(Request $request, User $user): RedirectResponse
    {
        if ($request->user()->isNot($user)) {
            abort(403);
        }
        $validWord = $request->validate(['text' => [
            'required',
            'string',
            // Must not contain any space characters
            'not_regex:/\s/',
            // The longest English word is Pneumonoultramicroscopicsilicovolcanoconiosis, 45 characters
            // See https://www.grammarly.com/blog/14-of-the-longest-words-in-english/
            'max:45',
        ]]);
        $request->user->words()->create($validWord);
        return redirect(route('users.words.index', [$user]));
    }

    /**
     * Delete the word
     */
    public function destroy(Request $request, Word $word)
    {
        if ($request->user()->isNot($word->user)) {
            abort(403);
        }
        $word->delete();
        return redirect(route('users.words.index', [$word->user]));
    }
}
