<?php

namespace App\Http\Controllers;

use App\Models\Word;
use Inertia\Inertia;
use Inertia\Response;

class WordController extends Controller
{
    /**
     * Display all users' words.
     */
    public function index(): Response
    {
        return Inertia::render('Words/Index', [
            'words' => Word::with('user')->latest()->get(),
        ]);
    }
}
