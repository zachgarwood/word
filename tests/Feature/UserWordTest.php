<?php

namespace Tests\Feature;

use App\Models\{User, Word};
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserWordTest extends TestCase
{
    use RefreshDatabase;

    public function test_display_only_users_words_by_most_recent()
    {
        $this->seed();
        $user = User::where(['email' => 'test@example.com'])->first();
        Word::factory()->count(3)->create(['user_id' => $user->id]);
        $words = Word::where(['user_id' => $user->id])->latest()->pluck('text')->toArray();
        $otherUsers = User::whereNot(['user_id' => $user->id])->pluck('name');

        $response = $this->actingAs($user)->get("/users/{$user->id}/words");
        $response->assertOk();
        $response->assertSeeInOrder($words);
        foreach ($otherUsers as $userName) {
            $response->assertDontSee($userName);
        }
    }

    public function test_user_can_only_post_words_for_self()
    {
        $user = User::factory()->create();
        $otherUser = User::factory()->create();

        $response = $this->actingAs($user)->post("/users/{$otherUser->id}/words", ['text' => 'word']);
        $response->assertForbidden();

        $response = $this->actingAs($user)->post("/users/{$user->id}/words", ['text' => 'word']);
        $response->assertRedirect("/users/{$user->id}/words");
    }

    public function test_user_can_only_delete_words_for_self()
    {
        $user = User::factory()->has(Word::factory())->create();
        $word = $user->words()->first();
        $otherUser = User::factory()->has(Word::factory())->create();
        $otherWord = $otherUser->words()->first();

        $response = $this->actingAs($user)->delete("/words/{$otherWord->id}");
        $response->assertForbidden();

        $response = $this->actingAs($user)->delete("/words/{$word->id}");
        $response->assertRedirect("/users/{$user->id}/words");
    }
}
