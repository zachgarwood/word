<?php

namespace Tests\Feature;

use App\Models\{User, Word};
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WordsTest extends TestCase
{
    use RefreshDatabase;

    public function test_display_all_words_by_most_recent()
    {
        $this->seed();
        $user = User::where(['email' => 'test@example.com'])->first();
        $words = Word::latest()->pluck('text')->toArray();

        $response = $this->actingAs($user)->get('/words');
        $response->assertStatus(200);
        $response->assertSeeInOrder($words);
    }
}
