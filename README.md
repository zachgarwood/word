Word
=====

A twitter-clone that only allows posting one word at a time.

# How to Run

## Prerequisites
You will need the following programs in order to install and run the application:
- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [PHP](https://www.php.net/manual/en/install.php)
- [Composer](https://getcomposer.org/doc/00-intro.md)
- [NodeJS](https://nodejs.org/en/download/)
- [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
- [SQLite](https://www.sqlite.org/download.html)

## Install
1. Pull down the code: `git clone git@gitlab.com:zachgarwood/word.git .`
2. Install dependencies: `composer install && npm install`

## Configure
1. Copy the example configuraton: `cp .env.example .env`
2. Generate an application key: `php artisan key:generate`

## Migrate
1. Create the database file: `touch database/database.sqlite`
2. Migrate and seed the database: `php artisan migrate --seed`

## Build
Build the assets: `npm run build`

## Serve
Serve the application: `php artisan serve`

The application should now be running at the url http://127.0.0.1:8000.

# How to Test

## Configure
1. Copy the example configuration: `cp .env.example .env.testing`
2. Add this line to the new `.env.testing` file: `DB_DATABASE=database/testing.sqlite`
3. Generate an application key: `php artisan key:generate --env=testing`

## Migrate
Create the database file: `touch database/testing.sqlite`

## Run
Run the tests: `php artisan test`
