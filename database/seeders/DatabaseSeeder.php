<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{User, Word};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create(['name' => 'Test User', 'email' => 'test@example.com']);
        User::factory()
            ->has(Word::factory()->count(10))
            ->count(2)
            ->create();
    }
}
